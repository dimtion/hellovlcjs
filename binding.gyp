{
  "targets": [
    {
      "target_name": "hellovlc",
      "include_dirs": [
        "lib/libvlcpp",
        "<!(node -e \"require('nan')\")",
      ],
      "sources": [ 
        "src/hellovlc.cc",
        "src/addon.cc",
      ],
      "cflags!": [ "-fno-exceptions" ],
      "cflags_cc!": [ "-fno-exceptions" ],
      "link_settings": {
        "libraries": [
            "-lvlc",
        ], 
        "library_dirs": [
            "/usr/local/lib",
        ],
      }
    }
  ]
}
