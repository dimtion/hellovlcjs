#ifndef HELLOVLC_H
#define HELLOVLC_H

#include <nan.h>
#include <node_object_wrap.h>
#include <vlcpp/vlc.hpp>

class HelloVlc : public node::ObjectWrap {
  public:
    static void Init(v8::Local<v8::Object> exports);
    static void NewInstance(const v8::FunctionCallbackInfo<v8::Value>& args);

    void* lock(void** pBuffer);
    void unlock(void* pictureId, void*const* planes);
    void display(void* pictureId);
    v8::Local<v8::ArrayBuffer> imageBuffer;
    v8::Local<v8::Uint8Array> image;
    v8::Persistent<v8::Function> drawCb;
    char* imageData;
    v8::Isolate* iso;

  private:
    // explicit HelloVlc();
    explicit HelloVlc(v8::Local<v8::Function> drawCallback);
    ~HelloVlc();

    static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void Play(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void Pause(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void Draw(const v8::FunctionCallbackInfo<v8::Value>& args);
    void draw(const v8::FunctionCallbackInfo<v8::Value>& args);

    static v8::Persistent<v8::Function> constructor;
    VLC::Instance vlcInstance;
    VLC::Media vlcMedia;
    VLC::MediaPlayer vlcMediaPlayer;

};

#endif
