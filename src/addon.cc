// addon.cc
#include <nan.h>
#include "hellovlc.h"

using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Object;
using v8::String;
using v8::Value;

void CreateObject(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    Local<Object> obj = Object::New(isolate);
    HelloVlc::NewInstance(args);

}

void InitAll(Local<Object> exports, Local<Object> module) {
    HelloVlc::Init(exports);

    // NODE_SET_METHOD(module, "exports", CreateObject);
}

NODE_MODULE(addon, InitAll)
