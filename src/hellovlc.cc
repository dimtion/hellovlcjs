#include <thread>
#include <node.h>
#include "hellovlc.h"

#include <iostream>

using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Object;
using v8::String;
using v8::Value;
using v8::Isolate;
using v8::Persistent;
using v8::Function;
using v8::FunctionTemplate;
using v8::Context;
using v8::Number;

Persistent<Function> HelloVlc::constructor;

// HelloVlc::HelloVlc() {
//     char const *argv[] = {
//         "--verbose",
//         "2",
//         "--no-xlib",
//     };
//     int argc = 3; 
//     vlcInstance = VLC::Instance(argc, argv);
//     vlcMedia = VLC::Media(vlcInstance, "/home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv", VLC::Media::FromPath);
//     vlcMediaPlayer = VLC::MediaPlayer(vlcMedia);
// }

HelloVlc::HelloVlc(v8::Local<v8::Function> drawCallback) {
    char const *argv[] = {
        "--verbose",
        "2",
        "--no-xlib",
    };
    int argc = 3; 
    vlcInstance = VLC::Instance(argc, argv);
    vlcMedia = VLC::Media(vlcInstance, "/home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv", VLC::Media::FromPath);
    vlcMediaPlayer = VLC::MediaPlayer(vlcMedia);

    // drawCb = drawCallback;
    // drawCb = v8::Persistent<Function>(v8::Isolate::GetCurrent(), drawCallback);
    // drawCb.Reset( v8::Isolate::GetCurrent(), drawCallback );

    iso = v8::Isolate::GetCurrent(); 
    std::cout << "Isolate args : " << v8::Isolate::GetCurrent() << std::endl;
    drawCb.Reset(v8::Isolate::GetCurrent(), drawCallback);

    std::cout << ">>>>>> HelloVlc With callback init" << std::endl;
    vlcMediaPlayer.setVideoCallbacks(
        [this](void** pBuffer) -> void* {
            return this->lock(pBuffer);
        }, [this](void* pictureId, void*const* planes) -> void {
            return this->unlock(pictureId, planes);
        }, [this](void* pictureId) -> void {
            return this->display(pictureId);
    });

    vlcMediaPlayer.setVideoFormatCallbacks([](char* chroma, uint32_t* width, uint32_t* height, uint32_t* pitch, uint32_t* lines) -> int {
        memcpy(chroma, "RV32", 4);
        *width = 1920;
        *height = 1080;
        *pitch = *width * 4;
        *lines = 1080;
        return 1;
    }, nullptr);


    size_t s = 1920*1080*4;  // 720*480*4;
    // imageData = malloc(s);
    imageData = (char*) malloc(s * sizeof(char));
    // imageBuffer = v8::ArrayBuffer::New(v8::Isolate::GetCurrent(), imageData, s);
    // image = v8::Uint8Array::New(imageBuffer, 0, s);
    //std::this_thread::sleep_for( std::chrono::seconds( 3 ) );
    std::cout << "Stop SLEEP" << std::endl;
}

HelloVlc::~HelloVlc () {
    std::cout << "HELLOVLC DESTROYED :(" << std::endl;
}

void* HelloVlc::lock(void** pBuffer) {
    std::cout << "Lock" << std::endl;
    // auto imgBuffer = malloc(720 * 480 * 4);
    // *pBuffer = image->Buffer()->GetContents().Data();
    // std::cout << image->Buffer()->GetContents().Data() << std::endl;
    // *pBuffer = imgBuffer;
    // free(imageData);
    // imageData = (char*) malloc(1920*1080*4);
    *pBuffer = imageData;
    std::cout << "Locked" << std::endl;
    return NULL;
}

void HelloVlc::unlock(void* pictureId, void*const* planes) {
    std::cout << "unlock" << std::endl;
}

void HelloVlc::display(void* pictureId) {
    //v8::Isolate* iso = v8::Isolate::New();
    // iso->Enter();
    std::cout << "display : " << pictureId << std::endl;
    std::cout << v8::Isolate::GetCurrent() << std::endl;
    const unsigned argc = 1;

    // Local<Value> argv[argc] = { image };
    // Local<Value> argv[argc] = { v8::String::NewFromUtf8(v8::Isolate::GetCurrent(), "Test") };
    // Local<Function> cb = v8::Local<Function>::New(v8::Isolate::GetCurrent(), drawCb);
    // cb->Call(Null(v8::Isolate::GetCurrent()), argc, argv);
}

void HelloVlc::Init(Local<Object> exports) {
    std::cout << "Init HelloVlc" << std::endl;
    Isolate* isolate = exports->GetIsolate();
    // prepare constructor
    Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
    tpl->SetClassName(String::NewFromUtf8(isolate, "HelloVlc"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    // Prototype
    NODE_SET_PROTOTYPE_METHOD(tpl, "Draw", Draw);
    NODE_SET_PROTOTYPE_METHOD(tpl, "Play", Play);
    NODE_SET_PROTOTYPE_METHOD(tpl, "Pause", Pause);

    constructor.Reset(isolate, tpl->GetFunction());
    exports->Set(String::NewFromUtf8(isolate, "HelloVlc"),
               tpl->GetFunction());
}

void HelloVlc::New(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    // std::cout << "This is life" << std::endl;

    Local<Function> callback = Local<Function>::Cast(args[0]);
    // isolate->Enter();

    // v8::Persistent<Function> drawCb;
    // auto drawCb = v8::Persistent<Function>::New(v8::Isolate::GetCurrent(), callback);
    // drawCb.Reset(v8::Isolate::GetCurrent(), callback);
    if (args.IsConstructCall()) {
        // Invoked as constructor: `new HelloVlc()`
        HelloVlc* obj = new HelloVlc(callback);
        obj->Wrap(args.This());
        args.GetReturnValue().Set(args.This());
    }

}

// void HelloVlc::NewInstance(const FunctionCallbackInfo<Value>& args) {
// 
//     Isolate* isolate = args.GetIsolate();
//     const unsigned argc = 1;
//     Local<Value> argv[argc] = { args[0] };
//     Local<Function> cons = Local<Function>::New(isolate, constructor);
//     Local<Context> context = isolate->GetCurrentContext();
//     Local<Object> instance =
//         cons->NewInstance(context, argc, argv).ToLocalChecked();
// 
//     args.GetReturnValue().Set(instance);
// }

void HelloVlc::draw(const FunctionCallbackInfo<Value>& args) {
    std::cout << "Draw" << std::endl;
    Isolate* isolate = args.GetIsolate();
    Local<Function> cb = Local<Function>::Cast(args[0]);
    const unsigned argc = 1;

    // uint8_t[]* image = new uint8_t[480*720*4];

    size_t s = 1920*1080*4;  // 720*480*4;
    Local<v8::ArrayBuffer> imageBuffer = v8::ArrayBuffer::New(isolate, s);
    Local<v8::Uint8Array> image = v8::Uint8Array::New(imageBuffer, 0, s);
    // char* imageData = (char*) malloc(s * sizeof(char));
    char* glBuffer  = (char*) node::Buffer::Data((v8::Local<v8::Object>) image);
    memcpy(glBuffer, imageData, s);
  
    // for(int i = 0; i < s; i+=4) {
    //     imageData[i]   = 127;
    //     imageData[i+1] = 10;
    //     imageData[i+2] = 255;
    //     imageData[i+3] = 255;
    // }
    // imageBuffer = v8::ArrayBuffer::New(v8::Isolate::GetCurrent(), 
    // auto imageBuffer = v8::ArrayBuffer::New(isolate, imageData, s);
    // auto image = v8::Uint8Array::New(imageBuffer, 0, s);
    
    // for(int i = 0; i < s; i+=1) {
    //     Local<Number> c = Number::New(isolate, imageData[i]);
    //     image->Set(i, c);
    //     // image->Set(i, Number::New(isolate, imageData[i]));
    //     // Local<Number> r = Number::New(isolate, 128);
    //     // Local<Number> g = Number::New(isolate, 0);
    //     // Local<Number> b = Number::New(isolate, 128);
    //     // Local<Number> a = Number::New(isolate, 255);
    //     // image->Set(i, r);
    //     // image->Set(i+1, g);
    //     // image->Set(i+2, b);
    //     // image->Set(i+3, a);
    // }
    
    // std::cout << int(image->Get(10021)->Int32Value()) << std::endl;

    Local<Value> argv[argc] = { image };
    cb->Call(Null(isolate), argc, argv);
    std::cout << "End draw" << std::endl;
}

void HelloVlc::Draw(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    HelloVlc* obj = ObjectWrap::Unwrap<HelloVlc>(args.Holder());
    obj->draw(args);
    // std::this_thread::sleep_for( std::chrono::seconds( 5 ) );
}

void HelloVlc::Play(const FunctionCallbackInfo<Value>& args) {
    // Isolate* isolate = args.GetIsolate();
    HelloVlc* obj = ObjectWrap::Unwrap<HelloVlc>(args.Holder());
    obj->vlcMediaPlayer.play();
}

void HelloVlc::Pause(const FunctionCallbackInfo<Value>& args) {
    HelloVlc* obj = ObjectWrap::Unwrap<HelloVlc>(args.Holder());
    obj->vlcMediaPlayer.pause();
}

/*
void Method(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    char const *argv[] = {
        "--verbose",
        "2",
        "--no-xlib",
    };
    int argc = 3; 
    auto instance = VLC::Instance(argc, argv);
    auto media = VLC::Media(instance, "/home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv", VLC::Media::FromPath);
    auto mp = VLC::MediaPlayer(media);
    mp.play();
    std::this_thread::sleep_for( std::chrono::seconds( 5 ) );
    // mp.stop();
    args.GetReturnValue().Set(String::NewFromUtf8(isolate, "world"));
}

void init(Local<Object> exports) {
    NODE_SET_METHOD(exports, "hello", Method);
}

NODE_MODULE(addon, init)
*/
