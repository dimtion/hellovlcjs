target=./build/Release/hellovlc.node

all: $(target)


build/:
	node-gyp configure

$(target): binding.gyp build/ src/*.cc src/*.h
	node-gyp rebuild  --target=1.6.11 --arch=x64 --dist-url=https://atom.io/download/electron

clean:
	rm -rf build/

.PHONY: clean
